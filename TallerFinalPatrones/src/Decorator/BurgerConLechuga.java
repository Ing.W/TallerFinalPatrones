package Decorator;

public class BurgerConLechuga implements Hamburguesa {
    private final Hamburguesa hamburguesa;

    public BurgerConLechuga(Hamburguesa hamburguesa){
        this.hamburguesa = hamburguesa;
    }
    @Override
    public String armarBurger(){
        this.hamburguesa.armarBurger();
        System.out.println("Lechuga agregada");
        return "";
    }
}
