package Decorator;
import Singleton.Orden;
import javax.swing.JOptionPane;



public class Main {
    public static void main(String[] args)
    {
        int opc;
        Orden orden = Orden.getSingleton();
        String p = " ";


        do {
            System.out.println("Ingrese su orden\n");
            System.out.println("Menu");
            System.out.println("opcion 1 - Hamburguesa con todo");
            System.out.println("");
            System.out.println("opcion 2 - Hamburguesa clasica ");
            System.out.println("");
            System.out.println("opcion 3 - Hamburgesa sin vegetales ");
            System.out.println("");
            opc = Integer.parseInt(JOptionPane.showInputDialog("Digite un numero"));
            limpiar(50); // no encontre otro metodo para limpiar pantalla, un console.clear

            switch (opc) {
                case 1 -> {
                    System.out.println("\nPreparando....");
                    BurgerConPan burgerConPan = new BurgerConPan();
                    BurgerConCarne burgerConCarne = new BurgerConCarne(burgerConPan);
                    BurgerConPollo burgerConPollo = new BurgerConPollo(burgerConCarne);
                    BurgerConArepa burgerConArepa = new BurgerConArepa(burgerConPollo);
                    BurgerConQueso burgerConQueso = new BurgerConQueso(burgerConArepa);
                    BurgerConLechuga burgerConLechuga = new BurgerConLechuga(burgerConQueso);
                    BurgerConTomate burgerConTomate = new BurgerConTomate(burgerConLechuga);
                    BurgerConTocineta burgerConTocineta = new BurgerConTocineta(burgerConTomate);
                    BurgerConSalsas burgerConSalsas = new BurgerConSalsas(burgerConTocineta);
                    orden.listaBurger.add(burgerConSalsas);
                    burgerConSalsas.armarBurger();
                    System.out.println("\n Listo :) ");
                }
                case 2 -> {
                    System.out.println("\nPreparando....");
                    BurgerConPan burgerConPan1 = new BurgerConPan();
                    BurgerConCarne burgerConCarne1 = new BurgerConCarne(burgerConPan1);
                    BurgerConQueso burgerConQueso1 = new BurgerConQueso(burgerConCarne1);
                    BurgerConLechuga burgerConLechuga1 = new BurgerConLechuga(burgerConQueso1);
                    BurgerConTomate burgerConTomate1 = new BurgerConTomate(burgerConLechuga1);
                    orden.listaBurger.add(burgerConTomate1);
                    burgerConTomate1.armarBurger();
                    System.out.println("\n Listo :) ");
                }
                case 3 -> {
                    System.out.println("\nPreparando....");
                    BurgerConPan burgerConPan2 = new BurgerConPan();
                    BurgerConCarne burgerConCarne2 = new BurgerConCarne(burgerConPan2);
                    BurgerConPollo burgerConPollo2 = new BurgerConPollo(burgerConCarne2);
                    BurgerConQueso burgerConQueso2 = new BurgerConQueso(burgerConPollo2);
                    BurgerConSalsas burgerConSalsas2 = new BurgerConSalsas(burgerConQueso2);
                    BurgerConTocineta burgerConTocineta2 = new BurgerConTocineta(burgerConSalsas2);
                    burgerConTocineta2.armarBurger();
                    orden.listaBurger.add(burgerConTocineta2);
                    System.out.println("\n Listo :) ");
                }
            }

            System.out.println("\nDesea algo mas joven?");
            p = JOptionPane.showInputDialog("Desea seguir ordenando? (si/no)");
            limpiar(50); // no encontre otro metodo para limpiar pantalla, un console.clear
            System.out.println("Ok");
        } while (p.equals("si"));


        System.out.println("\nDetalles de la orden\n");

        for (Hamburguesa o : orden.listaBurger) {

            System.out.println(" - " + o);
        }

        System.out.println("\n Gracias vuelva pronto <3");

    }

    private static void limpiar(int lineas) {
        for (int i = 0; i < lineas; i++) {
            System.out.println();
        }
    }
}
