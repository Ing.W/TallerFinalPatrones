package Decorator;

public class BurgerConTomate implements Hamburguesa {
    private final Hamburguesa hamburguesa;



    public BurgerConTomate(Hamburguesa hamburguesa){
        this.hamburguesa = hamburguesa;
    }
    @Override
    public String armarBurger(){
        this.hamburguesa.armarBurger();
        System.out.println("Tomate agregado");
        return "";
    }
    @Override
    public String toString() {
        return "Hamburguesa clasica";
    }
}
