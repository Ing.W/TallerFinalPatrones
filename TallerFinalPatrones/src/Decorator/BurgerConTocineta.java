package Decorator;

public class BurgerConTocineta implements Hamburguesa {
    private final Hamburguesa hamburguesa;

    public BurgerConTocineta(Hamburguesa hamburguesa){
        this.hamburguesa = hamburguesa;
    }
    @Override
    public String armarBurger(){
        this.hamburguesa.armarBurger();
        System.out.println("Tocineta agregada");
        return "";
    }
    @Override
    public String toString() {
        return "Hamburguesa sin vegetales";
    }

}
