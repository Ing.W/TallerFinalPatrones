package Decorator;

public class BurgerConSalsas implements Hamburguesa {
    private final Hamburguesa hamburguesa;

    public String nombre = "totototot";

    public BurgerConSalsas(Hamburguesa hamburguesa){
        this.hamburguesa = hamburguesa;
    }
    @Override
    public String armarBurger(){
        this.hamburguesa.armarBurger();
        System.out.println("Salsas agregadas");
        return "";
    }

    @Override
    public String toString() {
        return "Hamburguesa con todo";
    }
}
