package Decorator;

public class BurgerConCarne implements Hamburguesa {
    private final Hamburguesa hamburguesa;

    public BurgerConCarne(Hamburguesa hamburguesa){
        this.hamburguesa = hamburguesa;
    }
    @Override
    public String armarBurger(){
        this.hamburguesa.armarBurger();
        System.out.println("Carne agregada");
        return "";
    }
}
