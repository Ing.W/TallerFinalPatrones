package Decorator;

public class BurgerConPollo implements Hamburguesa {
    private final Hamburguesa hamburgesa;

    public BurgerConPollo(Hamburguesa hamburgesa){
        this.hamburgesa = hamburgesa;
    }
    @Override
    public String armarBurger(){
        this.hamburgesa.armarBurger();
        System.out.println("Pollo agregado");
        return "";
    }
}
