package Decorator;

public class BurgerConArepa implements Hamburguesa {
    private final Hamburguesa hamburguesa;

    public BurgerConArepa(Hamburguesa hamburguesa){
        this.hamburguesa = hamburguesa;
    }
    @Override
    public String armarBurger(){
        this.hamburguesa.armarBurger();
        System.out.println("Arepa agregada");
        return "Añadir Piña";
    }
}
