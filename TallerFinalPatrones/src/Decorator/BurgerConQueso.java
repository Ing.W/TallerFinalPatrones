package Decorator;

public class BurgerConQueso implements Hamburguesa {
    private final Hamburguesa hamburguesa;

    public BurgerConQueso(Hamburguesa hamburguesa){
        this.hamburguesa = hamburguesa;
    }
    @Override
    public String armarBurger(){
        this.hamburguesa.armarBurger();
        System.out.println("Queso agregado");
        return "";
    }
}
