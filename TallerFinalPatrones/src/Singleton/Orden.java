package Singleton;

import Decorator.Hamburguesa;
import Decorator.Main;

import java.util.ArrayList;
import java.util.List;

public class Orden {

    public List<Hamburguesa> listaBurger = new ArrayList<>();
    private static Orden orden;// objeto tipo singleton

    //constructor
    public Orden(){
    }

    //metodo tipo objeto
    public static Orden getSingleton(){
        if(orden == null){
            orden = new Orden();
        }
        return orden;
    }
    //getter
    public List getList(){
        return listaBurger;
    }
    //setter
    public void setList(List list){
        this.listaBurger = list;
    }



}
